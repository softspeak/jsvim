set number
set nocompatible
set clipboard=unnamed
call pathogen#infect('/Users/jorgen/.drush/vimrc/bundle')
call pathogen#infect('~/.drush/bundle/drupalvim/bundle/vim-plugin-for-drupal{}')
call pathogen#infect('/Users/jorgensandstrom/.vim/bundle{}')
filetype plugin on
call pathogen#helptags()
execute pathogen#infect()
set backupdir=~/.vim_runtime/backupdir//
set directory=~/.vim_runtime/swapfiles//

" Vdebug stuff
let g:vdebug_options = {}
" let g:vdebug_options["break_on_open"] = 0

" Drupal stuff
set expandtab
set tabstop=2
set shiftwidth=2
set autoindent
set smartindent
if has("autocmd")
  " Drupal *.module and *.install files.
  augroup module
    autocmd BufRead,BufNewFile *.module set filetype=php.drupal
    autocmd BufRead,BufNewFile *.install set filetype=php.drupal
    autocmd BufRead,BufNewFile *.test set filetype=php.drupal
    autocmd BufRead,BufNewFile *.inc set filetype=php.drupal
    autocmd BufRead,BufNewFile *.profile set filetype=php.drupal
    autocmd BufRead,BufNewFile *.view set filetype=php.drupal
  augroup END
endif
syntax on

" Miss Mary Specific TAGS stuff
au BufNewFile,BufRead ~/Documents/distro/* set tags=~/Documents/distro/drupal.tags
au BufNewFile,BufRead ~/Documents/distro/* let $DRUPAL_ROOT='$HOME/Documents/distro/_www'
let g:Drupal_dirs = {7: '/Users/jorgensandstrom/Documents/distro/_www'}

" YCM & Snipmate
imap <C-Space> <Plug>snipMateShow
imap <C-Space><C-Space> <Plug>snipmateTriggerSnippet

"let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
"let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
"let g:SuperTabDefaultCompletionType = '<C-n>'

" You Complete Me stuff
autocmd FileType php set omnifunc=phpcomplete#CompletePHP

" Linting
let g:phpfmt_autosave = 0
let g:phpfmt_standard = 'Drupal'

colorscheme desert

" Syntastic stuff
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" General Remappings
" Move remappings
vmap  K <Plug>MoveBlockUp
vmap  J <Plug>MoveBlockDown
nmap  K <Plug>MoveLineUp
nmap  J <Plug>MoveLineDown


" Insert Mode Remappings
inoremap II <Esc>I
inoremap AA <Esc>A
inoremap OO <Esc>O
inoremap CC <Esc>C
inoremap SS <Esc>S
inoremap DD <Esc>dd
inoremap UU <Esc>u
inoremap KK <Esc>K<C-w><C-p>a

" Window Remappings
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-h> <C-w>h
noremap <C-l> <C-w>l

" Tab Remappings
noremap <D-1> 1gt
noremap <D-2> 2gt
noremap <D-3> 3gt
noremap <D-4> 4gt
noremap <D-5> 5gt
noremap <D-6> 6gt
noremap <D-7> 7gt
noremap <D-8> 8gt
noremap <D-9> 9gt
noremap <D-0> :tablast<DR>

" Center cursor on screen and adjust scrolloff
if !exists('*VCenterCursor')
  augroup VCenterCursor
  au!
  au OptionSet *,*.*
    \ if and( expand("<amatch>")=='scrolloff' ,
    \         exists('#VCenterCursor#WinEnter,WinNew,VimResized') )|
    \   au! VCenterCursor WinEnter,WinNew,VimResized|
    \ endif
  augroup END
  function VCenterCursor()
    if !exists('#VCenterCursor#WinEnter,WinNew,VimResized')
      let s:default_scrolloff=&scrolloff
      let &scrolloff=winheight(win_getid())/2
      au VCenterCursor WinEnter,WinNew,VimResized *,*.*
        \ let &scrolloff=winheight(win_getid())/2
    else
      au! VCenterCursor WinEnter,WinNew,VimResized
      let &scrolloff=s:default_scrolloff
    endif
  endfunction
endif

nnoremap zz :call VCenterCursor()<CR>

" Search for selected text, forwards or backwards.
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>

" Yaml syntax
let g:ycm_collect_identifiers_from_tags_files = 1
au BufNewFile,BufRead *.yaml,*.yml so ~/.vim/bundle/vim-yaml/after/syntax/yaml.vim
au BufNewFile,BufRead *.make so ~/.drush/vimrc/bundle/vim-plugin-for-drupal/syntax/drini.vim
